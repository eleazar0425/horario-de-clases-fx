package horario.clases.fx.controller;

import horario.clases.fx.view.AñadirMateriaDialog;
import horario.clases.fx.view.Dialog;
import javafx.fxml.FXML;
import javafx.scene.control.MenuItem;

//CLASE CONTROLADORA DE LOS EVENTOS DE LOS MENU ITEMS DEL PROGRAMA

public class RootLayoutController {

	private AñadirMateriaDialog amd;
	
	@FXML
	MenuItem addMateria, salir, addHorario, acercaDe, eliminar;
	
	//EVENTO SALIR
	@FXML
	private void salir(){
		System.exit(0);
	}
	
	//EVENTO AL PULSAR EL BOTON ANADIR MATERIA
	@FXML
	private void addMateria(){
		amd = new AñadirMateriaDialog();
		amd.show();
	}
	
	//EVENTO AL PULSAR EL BOTON ANADIR HORARIO
	@FXML
	private void addHorario(){
		AñadirHorarioPaneController ahpc = new AñadirHorarioPaneController();
		ahpc.initAñadirHorarioPane();
	}
	
	//EVENTO AL PULSAR EL BOTON ACERCA DE
	@FXML
	private void acercaDe(){
		AcercaDeController acercaDe = new AcercaDeController();
		acercaDe.initAcercaDePane();
	}
	
	//EVENTO AL PULSAR EL BOTON ELIMINAR MATERIA
	@FXML
	private void eliminarMateria(){
		Dialog dialog = new Dialog("Eliminar Materia","ADVERTENCIA: Asegurate de no estar usando la materia en ningun horario, antes de eliminarla","Selecciona la materia");
		dialog.showDeleteMateria();
	}
	
	//EVENTO AL PULSAR OPCION AUN NO DISPONIBLE
	@FXML
	private void noDisponible(){
		Dialog dialog = new Dialog("En construcción","Ups! Estamos tirando codigos para que tengas pronto esta opción!","En la proxima beta estará disponible!");
		dialog.Show();
	}
}
