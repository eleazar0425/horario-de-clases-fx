package horario.clases.fx.controller;

import java.io.IOException;
import java.util.Vector;

import horario.clases.fx.MainApp;
import horario.clases.fx.model.ControladorDeDias;
import horario.clases.fx.model.HorarioModel;
import horario.clases.fx.model.MateriaDB;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;

//CLASE CONTROLADORA DEL DIALOG ANADIR HORARIO

public class AņadirHorarioPaneController {
	
	String[] dias;
	Vector<String> materias;
	Integer[] horas;
	Integer[] minutos;
	
	Stage stage = new Stage();
	Pane pane;
	
	private HorarioModel horario;
	
	@FXML
	private TextField profesor, curso;
	
	@FXML
	private ComboBox<String> materiaBox;
	
	@FXML
	private ComboBox<String> diaBox;
	
	@FXML
	private ComboBox<Integer> horaBox, horaFinalBox;
	
	@FXML
	private ComboBox<Integer> minutoBox, minutoFinalBox;
	
	@FXML
	private Button okey, cancelar;
	
	static MateriaDB materiadb;
	
	static AņadirHorarioPaneController ahpc = new AņadirHorarioPaneController();
	
	public AņadirHorarioPaneController(){
		horas = new Integer[24];
		minutos = new Integer[60];
		
		materiadb = new MateriaDB();
		
		String[] dias = {"Lunes", "Martes", "Miercoles", "Jueves", "Viernes", "Sabado", "Domingo"};
		this.dias = dias;
		
		for(int i=0; i<minutos.length; i++){
			minutos[i] = i;
		}
		
		for(int i=0; i<horas.length; i++){
			horas[i] = i;
		}
		
		materias = materiadb.getAllMaterias();
		
		ObservableList<String> materias = FXCollections.observableArrayList();
		materiaBox = new ComboBox<String>();
	
		ObservableList<String> dias1 = FXCollections.observableArrayList();
		diaBox = new ComboBox<String>(dias1);
		
		ObservableList<Integer> hora = FXCollections.observableArrayList();
		horaBox = new ComboBox<Integer>(hora);
		horaFinalBox = new ComboBox<Integer>(hora);
		
		ObservableList<Integer> minuto = FXCollections.observableArrayList();
		minutoBox = new ComboBox<Integer>(minuto);
		minutoFinalBox = new ComboBox<Integer>(minuto);
		
		profesor = new TextField();
		curso = new TextField();
		
		stage.setTitle("Aņadir horario");
		
		cancelar = new Button();
		okey = new Button();
		
		horario = new HorarioModel();
	}
	
	@FXML
	public void initialize(){
		diaBox.getItems().addAll(dias);
		diaBox.setValue(dias[ControladorDeDias.getActualDay()-1]);
		
		materiaBox.getItems().clear();
		materiaBox.getItems().addAll(materias);
		materiaBox.setValue(materias.get(0));
		
		horaBox.getItems().addAll(horas);
		horaBox.setValue(horas[11]);
		horaFinalBox.getItems().addAll(horas);
		horaFinalBox.setValue(horas[13]);
		
		minutoBox.getItems().addAll(minutos);
		minutoBox.setValue(minutos[29]);
		minutoFinalBox.getItems().addAll(minutos);
		minutoFinalBox.setValue(minutos[29]);
		
		profesor.setText("");
		curso.setText("");
	}
	
	public void initAņadirHorarioPane(){
		try{
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/AnadirHorarioPane.fxml"));
			pane = (Pane)loader.load();
			ahpc = (AņadirHorarioPaneController) loader.getController();
			Scene scene = new Scene(pane);
			stage.setScene(scene);
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setResizable(false);
			Image image = new Image("file:res/icon-medium.png");
			stage.getIcons().add(image);
			stage.show();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public void initAņadirHorarioPane(String nombre){
		try{
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/AnadirHorarioPane.fxml"));
			pane = (Pane)loader.load();
			ahpc = (AņadirHorarioPaneController) loader.getController();
			Scene scene = new Scene(pane);
			stage.setScene(scene);
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setResizable(false);
			materiaBox.getItems().clear();
			materiaBox.getItems().addAll(materias);
			materiaBox.valueProperty().setValue(nombre);
			stage.show();
		}catch(IOException e){
			e.printStackTrace();
		}
	}

	public void imprimirHorario(){
		System.out.println(horario.getDia());
	}
	
	@FXML
	private void handleCloseButtonAction(ActionEvent event) {
	    Stage stage = (Stage) cancelar.getScene().getWindow();
	    stage.close();
	}
	
	/*Metodo que funciona como evento al pulsar el boton okey, Obtiene toda la informacion compleata por
	*el usuario, y aņade un nuevo horario a la base de datos*/
	@FXML
	private void addHorario(ActionEvent event){
		String horaInicial, horaFinal, minutoInicial, minutoFinal;
		
		horaInicial = horaBox.getSelectionModel().getSelectedItem().toString();
		horaFinal = horaFinalBox.getSelectionModel().getSelectedItem().toString();
		minutoInicial = minutoBox.getSelectionModel().getSelectedItem().toString();
		minutoFinal = minutoFinalBox.getSelectionModel().getSelectedItem().toString();

		horario.setHoraInicio(reFormatHour(horaInicial, minutoInicial));
		horario.setHoraFinal(reFormatHourFinal(horaFinal, minutoFinal));
		horario.setDia(diaBox.getSelectionModel().getSelectedIndex());
		horario.setProfesor(profesor.getText());
		horario.setCurso(curso.getText());
		horario.setIdUsuario(1);
		horario.setNombreMateria(materiaBox.getSelectionModel().getSelectedItem().toString());
		
		System.out.println(horario.getDia());
		MateriaDB materia = new MateriaDB();
		Boolean status = materia.addNewHorario(horario);
		
		Stage stage = (Stage) cancelar.getScene().getWindow();
		
	    if(status==true){
	    	stage.close();    	
	    	MainApp.getMainWindowController().refresh(horario.getDia());
	    }
	}
	
	//Metodo para obtener el controllador de este pane
	public static AņadirHorarioPaneController getController(){
		return
				ahpc;
	}
	
	/*Metodo para darle un formato especifico a la hora. En este caso sumarle siempre un segundo a 
	*la hora inicial. Se hace esto para evitar que el programa lanze un error de choques innecesariamente.
	*Por ejemplo cuando tenemos una materia que termina a una hora, pero inmediatamente, a esa misma hora
	*iniciamos con otra materia. Esto se hace para evitar que el programa detecte coques de horarios en 
	*ese tipo de casos. A la hora final no se le puede aņadir nunca el segundo extra.
	*/
	private String reFormatHour(String hour, String minutes){
		int minutesValue = Integer.parseInt(minutes);
		int hourValue = Integer.parseInt(hour);
		if(minutesValue<10){
			minutes = "0"+minutes;
		}
		if(hourValue<10){
			hour = "0"+hour;
		}
		String hour2 = hour+":"+minutes+":01";
		return
				hour2;
	}
	
	private String reFormatHourFinal(String hour, String minutes){
		int minutesValue = Integer.parseInt(minutes);
		int hourValue = Integer.parseInt(hour);
		if(minutesValue<10){
			minutes = "0"+minutes;
		}
		if(hourValue<10){
			hour = "0"+hour;
		}
		String hour2 = hour+":"+minutes+":00";
		return
				hour2;
	}
}
