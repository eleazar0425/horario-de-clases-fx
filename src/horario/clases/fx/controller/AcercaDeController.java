package horario.clases.fx.controller;

import horario.clases.fx.MainApp;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
// CLASE CONTROLADORA DE LA VENTA ACERCA DE
 
public class AcercaDeController {
	Stage stage = new Stage();
	AnchorPane pane;
	
	public void initAcercaDePane(){
		try{
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/AcercaDe.fxml"));
			pane = (AnchorPane)loader.load();
			Scene scene = new Scene(pane);
			stage.setScene(scene);
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.setResizable(false);
			Image image = new Image("file:res/icon-medium.png");
			stage.getIcons().add(image);
			stage.show();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
}
