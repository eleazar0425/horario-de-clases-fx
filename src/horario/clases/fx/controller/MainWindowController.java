package horario.clases.fx.controller;

import eu.hansolo.enzo.notification.Notification.Notifier;
import eu.hansolo.enzo.notification.NotificationEvent;
import horario.clases.fx.MainApp;
import horario.clases.fx.model.ControladorDeDias;
import horario.clases.fx.model.Materia;
import horario.clases.fx.model.MateriaDB;
import horario.clases.fx.view.Dialog;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.ComboBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;

public class MainWindowController {
	
	int usuarioDefecto = 1; //usuario por defecto
	MateriaDB materiaDB = new MateriaDB();
	Dialog dialog;
	ContextMenu contextMenu;
	MenuItem eliminar, guardar;
	
	enum SELECTEDBOTTOM {B1, B2, B3, B4, B5, B6, B7}; //Enum para saber cual el ultimo boton pulsado
	SELECTEDBOTTOM actualBotton;
	
	@FXML
	Button b1, b2, b3, b4, b5, b6, b7;
	
	@FXML
	private Button anterior, siguiente;
	
	@FXML
	private Label label;
	
	@FXML
	private TableView<Materia> table = new TableView<Materia>();
	
	@FXML
	private TableColumn<Materia, String> hora;
	
	@FXML
	private TableColumn<Materia, String> nombre;
	
	@FXML
	private TableColumn<Materia, String> curso;
	
	@FXML
	private TableColumn<Materia, String> profesor;
	
	private int diaActual = 0; 
	
	Notifier notifier;
	
	@SuppressWarnings("unchecked")
	public MainWindowController(){
		
		hora = new TableColumn<Materia, String>("Hora");
		nombre = new TableColumn<Materia, String>("Materia");
		curso = new TableColumn<Materia, String>("Curso");
		profesor = new TableColumn<Materia, String>("Profesor");
		table.getColumns().addAll(hora, nombre, curso, profesor);
		anterior = new Button();
		siguiente = new Button();
		
		label = new Label();
		dialog = materiaDB.getNextMateria(usuarioDefecto, (ControladorDeDias.getActualDay()-1));
		
		b1 = new Button();
		b2 = new Button();
		b3 = new Button();
		b4 = new Button();
		b5 = new Button();
		b6 = new Button();
		b7 = new Button();
		
		anterior = new Button();
		siguiente = new Button();
	}
	
	@FXML
	public void initialize(){
		
		hora.setCellValueFactory(new PropertyValueFactory<Materia, String>("hora"));
		nombre.setCellValueFactory(new PropertyValueFactory<Materia, String>("nombre"));
		curso.setCellValueFactory(new PropertyValueFactory<Materia, String>("curso"));
		profesor.setCellValueFactory(new PropertyValueFactory<Materia, String>("profesor"));
		
		tablaEditable();
		addContextMenu();
		
		diaActual = (ControladorDeDias.getActualDay()-1);
		
		setDay(diaActual);
		setSelectecBotton(diaActual);
		
		try{
			dialog.Show();
		}catch(NullPointerException e){
			Boolean check = materiaDB.areThereMaterias(usuarioDefecto, diaActual);
			if(check==true){
				dialog = new Dialog("Has acabado por hoy", "Ya no tiene materias por hoy", "Ha finalizado tu dia");
				dialog.Show();
			}else{
				dialog = new Dialog("A�n no has a�adido ning�n horario!", "No tienes ning�n horario a�adido en este dia.", "Por favor agrega un horario");
				dialog.Show();
			}
		}
		
		addListeners();
		
		notificationThread(usuarioDefecto, diaActual).start();
	}
	
	private void tablaEditable(){
		ObservableList<String> ob = FXCollections.observableArrayList();
		ob.addAll(materiaDB.getAllMaterias());
		
		nombre.setCellFactory(ComboBoxTableCell.forTableColumn(ob));
		nombre.setOnEditCommit(new EventHandler<CellEditEvent<Materia, String>>(){

			@Override
			public void handle(CellEditEvent<Materia, String> e) {
				Materia viejoNombre = (Materia) e.getTableView().getItems().get(e.getTablePosition().getRow());
				System.out.println(viejoNombre.getNombre());
				String nuevoNombre = e.getNewValue();
				System.out.println(nuevoNombre);
				String profesor = viejoNombre.getProfesor();
				
				int dia = diaActual;
				
				final int dia2 = dia;
				guardar.setOnAction(new EventHandler<ActionEvent>(){
					@Override
					public void handle(ActionEvent arg0) {
						materiaDB.actualizarCampoMateria(dia2, viejoNombre.getNombre(), nuevoNombre, profesor);
						refresh();
					}
				});
			}
		});
		
		curso.setCellFactory(TextFieldTableCell.<Materia>forTableColumn());
		curso.setOnEditCommit(new EventHandler<CellEditEvent<Materia, String>>(){

			@Override
			public void handle(CellEditEvent<Materia, String> e) {

				Materia materia = (Materia) e.getTableView().getItems().get(e.getTablePosition().getRow());
				String viejoCurso = materia.getCurso();
				String nuevoCurso = e.getNewValue();
				String nombreMateria = materia.getNombre();
				
				final int dia2 = diaActual;
				guardar.setOnAction(new EventHandler<ActionEvent>(){
					@Override
					public void handle(ActionEvent arg0) {
						materiaDB.actualizarCampo(dia2, "curso", viejoCurso, nuevoCurso, nombreMateria);
						refresh();
					}
				});
			}
		});
		
		profesor.setCellFactory(TextFieldTableCell.<Materia>forTableColumn());
		profesor.setOnEditCommit(new EventHandler<CellEditEvent<Materia, String>>(){

			@Override
			public void handle(CellEditEvent<Materia, String> e) {

				Materia materia = (Materia) e.getTableView().getItems().get(e.getTablePosition().getRow());
				String viejoProfesor = materia.getProfesor();
				String nuevoProfesor = e.getNewValue();
				String nombreMateria = materia.getNombre();
				
				final int dia2 = diaActual;
				guardar.setOnAction(new EventHandler<ActionEvent>(){
					@Override
					public void handle(ActionEvent arg0) {
						materiaDB.actualizarCampo(dia2, "profesor", viejoProfesor, nuevoProfesor, nombreMateria);
						refresh();
					}
				});
			}
		});
	}
	
	private void addContextMenu(){
		
		eliminar = new MenuItem("Eliminar horario");
		guardar = new MenuItem("Guardar cambios");
		
		contextMenu = new ContextMenu();
		contextMenu.getItems().add(eliminar);
		contextMenu.getItems().add(guardar);
		
		table.setEditable(true);
		table.setContextMenu(contextMenu);
		
		eliminar.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				String nombre = table.getSelectionModel().getSelectedItem().getNombre();
				String hora = table.getSelectionModel().getSelectedItem().getHora().substring(0,5)+":01";
				int dia = diaActual;
				
				Dialog dialog = new Dialog("Eliminar Horario", "Est� seguro que desea eliminar "+nombre+" del horario de este dia?", "Presione okey para continuar");
				dialog.showDelete(nombre, dia, hora);
				
				refresh();
			}
		});
	}
	
	public void setDay(int dia){
		ObservableList<Materia> datos = FXCollections.observableArrayList();
		switch(dia){
		
		case 0:
			label.setText("Lunes");
			datos = materiaDB.getMaterias(usuarioDefecto, dia);
			table.setEditable(true);
			table.setItems(datos);
			System.out.println(diaActual);
			break;
			
		case 1:
			label.setText("Martes");
			datos = materiaDB.getMaterias(usuarioDefecto, dia);
			table.setEditable(true);
			table.setItems(datos);
			System.out.println(diaActual);
			break;
			
		case 2:
			label.setText("Miercoles");
			datos = materiaDB.getMaterias(usuarioDefecto, dia);
			table.setEditable(true);
			table.setItems(datos);
			System.out.println(diaActual);
			break;
			
		case 3:
			label.setText("Jueves");
			datos = materiaDB.getMaterias(usuarioDefecto, dia);
			table.setEditable(true);
			table.setItems(datos);
			System.out.println(diaActual);
			break;

		case 4:
			label.setText("Viernes");
			datos = materiaDB.getMaterias(usuarioDefecto, dia);
			table.setEditable(true);
			table.setItems(datos);
			System.out.println(diaActual);
			break;
			
		case 5:
			label.setText("Sabado");
			datos = materiaDB.getMaterias(usuarioDefecto, dia);
			table.setEditable(true);
			table.setItems(datos);
			System.out.println(diaActual);
			break;
			
		case 6:
			label.setText("Domingo");
			datos = materiaDB.getMaterias(usuarioDefecto, dia);
			table.setEditable(true);
			table.setItems(datos);
			System.out.println(diaActual);
			break;
		}
	}
	
	private void addListeners(){
		
		b1.setOnAction(new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent event) {
				diaActual=0;
				actualBotton = SELECTEDBOTTOM.B1;
				setDay(diaActual);
			}
		});
	
	
		b2.setOnAction(new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent event) {
				diaActual=1;
				actualBotton = SELECTEDBOTTOM.B2;
				setDay(diaActual);
			}
		});
		
		b3.setOnAction(new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent event) {
				diaActual=2;
				actualBotton = SELECTEDBOTTOM.B3;
				setDay(diaActual);
			}
		});
		
		b4.setOnAction(new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent event) {
				diaActual=3;
				actualBotton = SELECTEDBOTTOM.B4;
				setDay(diaActual);
			}
		});
		
		b5.setOnAction(new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent event) {
				diaActual=4;
				actualBotton = SELECTEDBOTTOM.B5;
				setDay(diaActual);
			}
		});
		
		b6.setOnAction(new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent event) {
				diaActual=5;
				actualBotton = SELECTEDBOTTOM.B6;
				setDay(diaActual);
			}
		});
		
		b7.setOnAction(new EventHandler<ActionEvent>(){
			@Override
			public void handle(ActionEvent event) {
				diaActual=6;
				actualBotton = SELECTEDBOTTOM.B7;
				setDay(diaActual);
			}
		});
	}
	
	private void setSelectecBotton(int actualDay){
		switch(actualDay){
		case 0:
			actualBotton = SELECTEDBOTTOM.B1;
			break;
			
		case 1:
			actualBotton = SELECTEDBOTTOM.B2;
			break;
			
		case 2:
			actualBotton = SELECTEDBOTTOM.B3;
			break;
			
		case 3:
			actualBotton = SELECTEDBOTTOM.B4;
			break;
			
		case 4:
			actualBotton = SELECTEDBOTTOM.B5;
			break;
			
		case 5:
			actualBotton = SELECTEDBOTTOM.B6;
			break;
			
		case 6:
			actualBotton = SELECTEDBOTTOM.B7;
			break;
		}
	}
	
	@FXML
	private void siguiente(){
		switch(actualBotton){
		
		case B1:
			System.out.println("Ahora se presenta el martes");
			setDay(1);
			actualBotton = SELECTEDBOTTOM.B2;
			break;
		
		case B2:
			System.out.println("Ahora se presenta el miercoles");
			setDay(2);
			actualBotton = SELECTEDBOTTOM.B3;
			break;	
			
		case B3:
			System.out.println("Ahora se presenta el Jueves");
			setDay(3);
			actualBotton = SELECTEDBOTTOM.B4;
			break;
		
		case B4:
			System.out.println("Ahora se presenta el Viernes");
			setDay(4);
			actualBotton = SELECTEDBOTTOM.B5;
			break;
			
			
		case B5:
			System.out.println("Ahora se presenta el Sabado");
			setDay(5);
			actualBotton = SELECTEDBOTTOM.B6;
			break;
			
		case B6:
			System.out.println("Ahora se presenta el Domingo");
			setDay(6);
			actualBotton = SELECTEDBOTTOM.B7;
			break;
			
		case B7:
			System.out.println("Ahora se sigue presentando el domingo");
			actualBotton = SELECTEDBOTTOM.B7;
			break;
		}
	}
	
	
	@FXML
	private void anterior(){
		switch(actualBotton){
		
		case B1:
			System.out.println("Se sigue presentando el lunes");
			actualBotton = SELECTEDBOTTOM.B1;
			break;
		
		case B2:
			setDay(0);
			System.out.println("Ahora se presenta el Lunes");
			actualBotton = SELECTEDBOTTOM.B1;
			break;	
			
		case B3:
			setDay(1);
			System.out.println("Ahora se presenta el Martes");
			actualBotton = SELECTEDBOTTOM.B2;
			break;
		
		case B4:
			setDay(2);
			System.out.println("Ahora se presenta el Miercoles");
			actualBotton = SELECTEDBOTTOM.B3;
			break;
			
		case B5:
			setDay(3);
			System.out.println("Ahora se presenta el Jueves");
			actualBotton = SELECTEDBOTTOM.B4;
			break;
			
		case B6:
			setDay(4);
			System.out.println("Ahora se presenta el Viernes");
			actualBotton = SELECTEDBOTTOM.B5;
			break;
			
		case B7:
			setDay(5);
			System.out.println("Ahora se sigue presentando el Sabado");
			actualBotton = SELECTEDBOTTOM.B6;
			break;
		}
	}
	
	public void refresh(){
		setDay(diaActual);
		setSelectecBotton(diaActual);
	}
	
	public void refresh(int dia){
		setDay(dia);
		setSelectecBotton(dia);
	}
	
	public Thread notificationThread(int idUsuario, int dia){
		Thread thread = new Thread(new Runnable(){

			@Override
			public void run() {
				
				System.out.println("Iniciando hilo");
				while(true){
					try {
						Thread.sleep(1000*60*1);
						String[] data = materiaDB.getNextMateriaString(idUsuario, dia);
						if(data[0]!=null){
							System.out.println("Pr�xima materia Tienes "+data[0]+" En el curso: "+data[1]+"En aproximadamente "+data[2]);
							System.out.println("Se ha iniciado el hilo");
							Platform.runLater(new Runnable(){
								@Override
								public void run() {
									showNotification("Tienes "+data[0], "en el curso "+data[1]+" en aprox. "+data[2]);
								}
							});
							Thread.sleep(1000*60*60*2);
						}else{
							System.out.println("Se ha dormido el hilo por 4 horas");
							Thread.sleep(1000*60*60*4);
						}
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		});
		return thread;
	}
	
	private void showNotification(String tittle, String info){
		
		String osName = System.getProperty("os.name");
		if(osName.equals("Windows 8.1") || osName.equals("Windows 10")){
			MainApp.sendWin10Notification(tittle, info);
		}else{
			Notifier notifier = Notifier.INSTANCE;
			notifier.notifyInfo(tittle, info);
			notifier.setOnHideNotification(new EventHandler<NotificationEvent>(){
				@Override
				public void handle(NotificationEvent arg0) {
					notifier.stop();
				}
			});
		}
	}
}