package horario.clases.fx;

import horario.clases.fx.controller.AńadirHorarioPaneController;
import horario.clases.fx.controller.MainWindowController;
import horario.clases.fx.frameworks.ScreensFramework;
import horario.clases.fx.model.MateriaDB;
import java.awt.AWTException;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.imageio.ImageIO;
import org.controlsfx.control.NotificationPane;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/*@author Eleazar Estrella
 * @version v2.1.2
 * @date 08-12-2015
 * 
 * HORARIO FX REALEASE BETA V2.1.2
 * COPYRIGHT TODOS LOS DERECHOS RESERVADOS
 * ESTA APPLICACION HA SIDO HECHA PARA FINES DE USO PERSONAL NO LUCRATIVO POR ELEAZAR ESTRELLA 
 */


public class MainApp extends Application {

	private  BorderPane rootLayout;
	private BorderPane mainWindow; 
	private static Stage primaryStage; //PrimaryStage estatico para poder tener acceso desde otras clases
	AńadirHorarioPaneController ahpc = new AńadirHorarioPaneController();
	static MainWindowController mwc; /*Una instancia estatica del controlador de la ventana principal
									*Para poder obtener el acceso a este en otras clases
									*/
	private static TrayIcon trayIcon;/*TrayIcon Statico para poder enviar notificaciones desde cualquier
	 								*clase. Igualmente con el NotificationPane
	 								*/
	private static NotificationPane notification;
	
	@Override 
	public void start(Stage primaryStage) throws IOException {
		Image image = new Image("file:res/icon-medium.png");
		MainApp.primaryStage = primaryStage;
		MainApp.primaryStage.setTitle("Horario de clases FX");
		MainApp.primaryStage.getIcons().add(image);
		MainApp.primaryStage.setResizable(false);
		
		Platform.setImplicitExit(false);
		
		createTryIcon(MainApp.primaryStage);
		
		ScreensFramework welcomeScreen = new ScreensFramework();
		MateriaDB materiadb = new MateriaDB();
		
		int status = materiadb.getUserStatus(1);
		
		if(status==0){/*Si el status del usuario es 0 (Lo que significa que es la primera vez que abre
			*la aplicacion) se abre la pantalla de bienvenida. De lo contrario inicia de forma normal
			*la aplicacion
			*/
			welcomeScreen.start();
		}else{
			initRootLayout();
			initMainWindow();
		}	
	}
	
	public MainApp(){
		Image image = new Image("file:res/icon-medium.png");
		primaryStage = new Stage();
		primaryStage.setTitle("Horario de clases FX");
		primaryStage.getIcons().add(image);
		primaryStage.setResizable(false);
	}

	public static void main(String[] args) {
		launch(args);
	}
	
	public void initRootLayout(){
		try{
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/RootLayout.fxml"));
			rootLayout = (BorderPane) loader.load();
			Scene scene = new Scene(rootLayout);
			
			primaryStage.setScene(scene);
			primaryStage.show();
			
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public void initMainWindow(){
		try{
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/MainWindow.fxml"));
			mainWindow = (BorderPane) loader.load();
			notification = new NotificationPane(mainWindow);
			notification.getStyleClass().add(NotificationPane.STYLE_CLASS_DARK);
			rootLayout.setCenter(notification);
			mwc = (MainWindowController) loader.getController();
		}catch(IOException e){
			e.printStackTrace();
		}
	}
	
	//Metodo para obtener el controlador de la ventana principal
	public static MainWindowController getMainWindowController(){
		return
				mwc;
	}
	
	//Metodo para crear el tray icon
	private void createTryIcon(Stage stage) throws IOException{
		 SystemTray tray = SystemTray.getSystemTray();
		 java.awt.Image image = ImageIO.read(MainApp.class.getResource("trayicon.png").openStream());
		 trayIcon = new TrayIcon(image);
		 
		 if(SystemTray.isSupported()){
			
			 //Evento para ocultar el primaryStage cuando se cierre la ventana
			 stage.setOnCloseRequest(new EventHandler<WindowEvent>(){
				@Override
				public void handle(WindowEvent arg0) {
					hide(stage);
				}
			 });
			 
			 //Evento para salir de la app desde menuitem del trayicon
			 ActionListener closeListener = new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent e) {
					System.exit(0);
				}
			 };
			 
			 //Evento para mostrar el stage nuevamente desde el trayicon
			 ActionListener showListener = new ActionListener(){
				@Override
				public void actionPerformed(ActionEvent e) {
					Platform.runLater(new Runnable(){
						@Override
						public void run() {
							stage.show();
						}
					});
				}
			 };
			 
			 PopupMenu popupMenu = new PopupMenu();
			 
			 MenuItem abrir = new MenuItem("Abrir Horario FX");
			 abrir.addActionListener(showListener);
			 popupMenu.add(abrir);
			 popupMenu.addSeparator();
			 
			 MenuItem salir = new MenuItem("Salir...");
			 salir.addActionListener(closeListener);
			 popupMenu.add(salir);
			 
			 trayIcon.setPopupMenu(popupMenu);
			 trayIcon.addActionListener(showListener);
			 
			 try {tray.add(trayIcon);}catch(AWTException e){e.printStackTrace();}
		 }
	}
	
	/*Metodo para ocultar el stage
	*NOTA: Es necesario usar Platform.runLater() para continuar en el hilo de java fx
	*/
	private void hide(final Stage stage){
		Platform.runLater(new Runnable(){
		@Override public void run() {
		stage.hide();
			}
		});
	}
	
	//Metodo para obtener el stage desde otras clases
	public static Stage getStage(){
		return MainApp.primaryStage;
	}
	
	//Metodo para enviar notificacion a equipos con Windows 8.1 en adelante
	public static void sendWin10Notification(String tittle, String info){
		 MainApp.trayIcon.displayMessage(tittle, info, TrayIcon.MessageType.INFO);
	}
	
	//Metodo para enviar y mostrar informacion a un NotificationPane
	public static void sendInfoPane(String text){
		notification.setGraphic(new ImageView("file:res/dialog-information.png"));
		notification.setText(text);
		notification.show();
		//Evento para que al mostrarse el NotificationPane, desaparezca despues de 4 segundos
		notification.setOnShown(new EventHandler<Event>(){
			@Override
			public void handle(Event arg0) {
				Platform.runLater(new Runnable(){
					@Override
					public void run() {
						try {
							Thread.sleep(4000);
							notification.hide();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
	}
}
