package horario.clases.fx.frameworks;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;

public class PageThreeController implements Initializable, ControlledScreen {

	ScreensController myController;
	@Override
	public void setScreenParent(ScreensController screenPage) {
		myController = screenPage;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
		
	}
	
	@FXML
	public void goToPageFour(){
		myController.setScreen(ScreensFramework.screen4ID);
	}

}
