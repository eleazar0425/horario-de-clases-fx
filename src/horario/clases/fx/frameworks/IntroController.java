package horario.clases.fx.frameworks;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;

public class IntroController implements Initializable, ControlledScreen {

	ScreensController myController;
	
	@Override
	public void setScreenParent(ScreensController screenPage) {
		myController = screenPage;
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		
	}

	@FXML
	public void goToPageOne(){
		myController.setScreen(ScreensFramework.screen1ID);
	}
}
