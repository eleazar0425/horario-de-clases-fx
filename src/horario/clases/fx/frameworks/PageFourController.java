package horario.clases.fx.frameworks;

import horario.clases.fx.MainApp;
import horario.clases.fx.model.MateriaDB;
import horario.clases.fx.view.AņadirMateriaDialog;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.Stage;

public class PageFourController implements Initializable, ControlledScreen {

			
	ScreensController myController;
	@Override
	public void setScreenParent(ScreensController screenPage) {
		myController = screenPage;
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
	}
	
	@FXML
	public void goToPageFour(){
		Stage stage = (Stage) myController.getScene().getWindow();
		stage.close();
		MainApp mainApp = new MainApp();
		mainApp.initRootLayout();
		mainApp.initMainWindow();
		AņadirMateriaDialog amd = new AņadirMateriaDialog();
		amd.show();
		MateriaDB materia = new MateriaDB();
		materia.setUserStatus(1, 1);
	}
}
