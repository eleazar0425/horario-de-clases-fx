package horario.clases.fx.model;

import horario.clases.fx.view.Dialog;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBConnection {
	
	public static Connection getConnection(){
		String ruta = "lib/horarioDB.db";
		Connection conect = null;
		try {
			Class.forName("org.sqlite.JDBC");
			conect = DriverManager.getConnection("jdbc:sqlite:"+ruta);
			
			System.out.println("Conexion realizada con exito");
		} catch (ClassNotFoundException | SQLException e) {
			Dialog dialog = new Dialog("ERROR GRAVE", "Error JDBC:SQLITE", "Clase no encontrada: "+ruta);
			dialog.Show();
			e.printStackTrace();
		}
		return conect;
	}
}