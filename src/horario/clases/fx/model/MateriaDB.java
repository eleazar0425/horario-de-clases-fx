package horario.clases.fx.model;

import horario.clases.fx.MainApp;
import horario.clases.fx.view.Dialog;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalTime;
import java.util.Vector;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class MateriaDB {
	
	private ObservableList<Materia> materia;
	private ResultSet rs;
	private Statement st;
	private ControladorDeHoras controladorhora;
	
	public ObservableList<Materia> getMaterias(int idUsuario, int dia){
		Connection conex = DBConnection.getConnection();
		String sql = "select Horario.horaInicio, Horario.horaFin, Materia.nombre, Horario.curso, Horario.profesor from Horario, Materia where Horario.idUsuario="+idUsuario
				+" and Horario.dia="+dia+" and Materia.idMateria = Horario.idMateria order by Horario.horaInicio";
		materia = FXCollections.observableArrayList();
		
		try{
			st = conex.createStatement();
			rs = st.executeQuery(sql);
			
			while(rs.next()){
				Materia mat = new Materia();
				
				String hora = rs.getString(1);
				String hora2 = rs.getString(2);
				String nombre = rs.getString(3);
				String curso = rs.getString(4);
				String profesor = rs.getString(5);
				
				mat = new Materia(formatHour(hora)+" a "+formatHour(hora2),nombre, curso, profesor);
				
				materia.add(mat);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return
				materia;
	}
	
	private String formatHour(String hour){
		String hour2 = hour.substring(0,5);
		return hour2;
	}
	
	public Dialog getNextMateria(int idUsuario, int dia){
		Connection conex = DBConnection.getConnection();
		controladorhora = new ControladorDeHoras();
		String now = controladorhora.getNow().substring(0,8);
		String sql = "select Materia.nombre, Horario.horaInicio, Horario.curso From Horario, Materia where "
				+ "Horario.idUsuario="+idUsuario+" and Horario.dia="+dia+" and Materia.idMateria "
						+ "= Horario.idMateria and Horario.horaInicio between '"+now+"' and '23:59:00' order by Horario.horaInicio Limit 1";
		
		controladorhora = new ControladorDeHoras();
		String curso = null;
		String hour = null;
		String title = null;
		String mensaje = null;
		LocalTime time = null;
		Dialog dialog = null;
		
		try{
			st = conex.createStatement();
			rs = st.executeQuery(sql);
			
			System.out.println(sql);
			while(rs.next()){
				 title = rs.getString(1);
				 hour = rs.getString(2);
				 curso = rs.getString(3);
				 time = LocalTime.parse(hour);
				mensaje = controladorhora.getIntervaloTiempo(time);

				if(mensaje!=null){
					dialog = new Dialog("Pr�xima materia","Tienes "+title+" en el curso: "+curso,"En aproximadamente "+mensaje);
				}else{
					Boolean check = areThereMaterias(idUsuario, dia);
					if(check==true){
						dialog = new Dialog("Has acabado por hoy", "Ya no tiene materias por hoy", "Ha finalizado tu dia");
					}else{
						dialog = new Dialog("A�n no has a�adido ning�n horario!", "No tienes ning�n horario a�adido en este dia.", "Por favor agrega un horario");
					}
				}
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		return dialog;
	}
	
	public String[] getNextMateriaString (int idUsuario, int dia){
		String[] data = new String[3];
		Connection conex = DBConnection.getConnection();
		controladorhora = new ControladorDeHoras();
		String now = controladorhora.getNow().substring(0,8);
		String sql = "select Materia.nombre, Horario.horaInicio, Horario.curso From Horario, Materia where "
				+ "Horario.idUsuario="+idUsuario+" and Horario.dia="+dia+" and Materia.idMateria "
						+ "= Horario.idMateria and Horario.horaInicio between '"+now+"' and '23:59:00' order by Horario.horaInicio Limit 1";
		
		controladorhora = new ControladorDeHoras();
		String curso = null;
		String hour = null;
		String title = null;
		String mensaje = null;
		LocalTime time = null;
		Dialog dialog = null;
		
		try{
			st = conex.createStatement();
			rs = st.executeQuery(sql);
			
			System.out.println(sql);
			while(rs.next()){
				 title = rs.getString(1);
				 hour = rs.getString(2);
				 curso = rs.getString(3);
				 time = LocalTime.parse(hour);
				mensaje = controladorhora.getIntervaloTiempo(time);

				if(mensaje!=null){
					data[0] = title;
					data[1] = curso;
					data[2] = mensaje;
				}else{
					data = null;
				}
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
		
		return data;
	}

	public void addMateria(String nombreMateria){
		Dialog dialog;
		Connection conex = DBConnection.getConnection();
		String sql = "insert into Materia values(null,'"+nombreMateria+"')";
		
		try{
			st = conex.createStatement();
			int confirmar = st.executeUpdate(sql);
			
			if(confirmar==1){
				
			}else{
				dialog = new Dialog("Error!", "Ha ocurrido un error al agregar la materia", "Error 0x001");
				dialog.showError();
			}
		}catch(SQLException e){
			dialog = new Dialog("Error!", e.getMessage(), "Error 0x002");
			dialog.showError();
		}
	}

	public Vector<String> getAllMaterias(){
		Vector<String> vector = new Vector<String>();
		
		Connection conex = DBConnection.getConnection();
		String sql = "select nombre from Materia";
		
		try{
			st = conex.createStatement();
			rs = st.executeQuery(sql);
			
			while(rs.next()){
				int i = 1;
				vector.add(rs.getString(i)) ;
				i++; 
			}
		
		}catch(SQLException e){
			e.printStackTrace();
		}
		 return vector;
	}
	
	public Boolean addNewHorario(HorarioModel horario){
		Dialog dialog;
		Connection conex = DBConnection.getConnection();
		String miniSql = "(Select idMateria from Materia where nombre='"+horario.getNombreMateria()+"')";
		
		String sql = "insert into Horario values("+miniSql+", "+horario.getDia()+", "+
		"'"+horario.getHoraInicio()+"', '"+horario.getHoraFinal()+"', '"+horario.getProfesor()+"', "+
		horario.getIdUsuario()+", '"+horario.getCurso()+"', null)";		
		
		String check = checkHorario(horario.getIdUsuario(), horario.getDia(), horario.getHoraInicio(), horario.getHoraFinal());
		
		if(check==null){
			try{
				st = conex.createStatement();
				int confirmar = st.executeUpdate(sql);
				
				if(confirmar==1){
					MainApp.sendInfoPane("Horario agregado correctamente!");
				}else{
					dialog = new Dialog("Error!", "Ha ocurrido un error al agregar el horario", "Error 0x003");
					dialog.showError();
				}
			}catch(SQLException ex){
				ex.printStackTrace();
			}
			
			return true;
		}else{
			dialog = new Dialog("Error!", "Ha ocurrido un error al agregar el horario", "Tienes un choque de materias");
			dialog.showError();
			return false;
		}
	}
	
	public void borrarHorario(int dia, String nombreMateria, String horaInicial){
		Connection conex = DBConnection.getConnection();
		
		Dialog dialog;
		String miniSql = "(select idMateria from materia where nombre='"+nombreMateria+"')";
		String sql = "delete from horario where dia="+dia+" and horaInicio='"+horaInicial+"' and idMateria="+miniSql;
		
		try {
			st = conex.createStatement();
			
			int confirmar = st.executeUpdate(sql);
			
			if(confirmar==1){
				MainApp.sendInfoPane("Horario borrado exitosamente!");
			}else{
				dialog = new Dialog("Error!", "Ha ocurrido un error al borrar el horario", "Error 0x003");
				dialog.showError();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			dialog = new Dialog("Error!", "Ha ocurrido un error al borrar el horario", "Error 0x003");
			dialog.showError();
		}
	}
	
	public void actualizarCampoMateria(int dia, String antiguoValor, String nuevoValor, String profesor){
		Connection conex = DBConnection.getConnection();
		String idMateriaNueva = "(select idMateria from materia where nombre='"+nuevoValor+"')";
		String idMateriaAntigua = "(select idMateria from materia where nombre='"+antiguoValor+"')";
		String sql = "update horario set idMateria="+idMateriaNueva+" where idMateria="+idMateriaAntigua+" and dia="+dia+" and profesor='"+profesor+"'";
		Dialog  dialog = new Dialog("Ha ocurrido un error al borrar la materia", "Ha ocurrido un error inesperado", "Presiona okey para continuar");

		try {
			st = conex.createStatement();
			int confirmar = st.executeUpdate(sql);
			if(confirmar==1){
				MainApp.sendInfoPane("Cambios guardados correctamente!");
			}else{
				dialog.showError();
			}
		} catch (SQLException e) {
			e.printStackTrace();
			dialog.showError();
		}
	}
	
	public void actualizarCampo(int dia, String campo, String antiguoValor, String nuevoValor, String nombreMateria){
		Connection conex = DBConnection.getConnection();
		Dialog dialog;
		String miniSql = "(select idMateria from Materia where nombre='"+nombreMateria+"')";
		String idHorario = "(select idHorario from horario where idMateria="+miniSql+" and "+campo+" = '"+antiguoValor+"' and dia="+dia+")";
		String sql = "update horario set "+campo+"='"+nuevoValor+"' where idHorario="+idHorario;
		
		try{
			st = conex.createStatement();
			
			int confirmar = st.executeUpdate(sql);
			
			if(confirmar==1){
				System.out.println("escrito en la base de datos");
				System.out.println(sql);
				MainApp.sendInfoPane("Cambios guardados correctamente!");
			}else{
				dialog = new Dialog("Ha ocurrido un error al borrar la materia", "Ha ocurrido un error inesperado", "Presiona okey para continuar");
				dialog.showError();
			}
		}catch(SQLException e){
			e.printStackTrace();
			dialog = new Dialog("Ha ocurrido un error al borrar la materia", "Ha ocurrido un error inesperado", "Presiona okey para continuar");
			dialog.showError();
		}
	}
	
	public int getUserStatus(int idUser){
		Connection conex = DBConnection.getConnection();
		int userStatus=1;
		String sql = "select status from usuario where idUsuario="+idUser;
		try{
			 st = conex.createStatement();
			 rs = st.executeQuery(sql);
			
			while(rs.next()){
				userStatus = rs.getInt(1);
			}
			System.out.println("El status del usuario es "+userStatus);
			
			rs.close();
			st.close();
			conex.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
		return userStatus;
	}
	
	public void setUserStatus(int idUsuario, int status){
		Connection conex = DBConnection.getConnection();
		String sql = "update usuario set status="+status+" where idUsuario="+idUsuario;
		
		try{
			st = conex.createStatement();
			int confirmar = st.executeUpdate(sql);
			
			if(confirmar==1){
				System.out.println("Se ha modificado el status del usuario correctamente");
			}else{
				System.out.println("Ha ocurrido un error, verificar que el status es cero o uno o que existe el idusuario");
			}
			
			st.close();
			conex.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	public void deleteMateria(String nombreMateria){
		Connection conex = DBConnection.getConnection();
		String sql = "delete from materia where nombre='"+nombreMateria+"'";
		Dialog dialog;
		try{
			st = conex.createStatement();
			int confirmar = st.executeUpdate(sql);
			if(confirmar==1){
				MainApp.sendInfoPane("Materia borrada exitosamente!");
			}else{
				dialog = new Dialog("Ha ocurrido un error al borrar la materia", "Ha ocurrido un error inesperado", "Presiona okey para continuar");
				dialog.showError();
			}
		}catch(SQLException e){
			e.printStackTrace();
		}
	}
	
	public String checkHorario(int idUsuario, int dia, String horaInicial, String horaFinal){
		String result = null;
		String sql = "select horaInicio, horaFin from horario where dia ="+dia+" and (horaInicio between'"+horaInicial+"' and '"+horaFinal+"' or horaFin between '"+horaInicial+"' and '"+horaFinal+"')";
	
		Connection conex = DBConnection.getConnection();
		
		try{
			st = conex.createStatement();
			rs = st.executeQuery(sql);
			while(rs.next()){
				 result = rs.getString(1);
			}
			System.out.println(sql);
			rs.close();
			st.close();
			conex.close();
		}catch(SQLException e){
			e.printStackTrace();
		}
		System.out.println("Resultado"+result);
		return result;	
	}
	
	public Boolean areThereMaterias(int idUsuario, int dia){
		Connection conex = DBConnection.getConnection();
		String sql = "select idHorario from horario where idUsuario="+idUsuario+" and dia="+dia+" limit 1";
		int check = -1;
		
		try{
			st = conex.createStatement();
			rs = st.executeQuery(sql);
			while(rs.next()){
				check = rs.getInt(1);
			}
			System.out.println(sql);
			rs.close();
			st.close();
			conex.close();
			
		}catch(SQLException e){
			e.printStackTrace();
		}
		
		if(check==-1){
			return false;
		}else{
			return true;
		}
	}
}