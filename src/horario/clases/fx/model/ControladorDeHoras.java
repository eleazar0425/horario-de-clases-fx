package horario.clases.fx.model;

import java.time.Duration;
import java.time.LocalTime;

//CLASE PARA EL MANEJO DE HORAS

public class ControladorDeHoras {

	private LocalTime time;
	private Duration duration;
	
	public ControladorDeHoras(){
		time = LocalTime.now();
	}
	
	//DEVUELVE LA HORA ACTUAL
	public String getNow(){
		return
				time.toString();
	}
	
	//DEVUELVE QUE CANTIDAD DE TIEMPO HAY ENTRE UN INTERVALO DE TIEMPO DESDE LA HOR ACTUAL.
	public String getIntervaloTiempo(LocalTime time){
		double horas = 0.0;
		String s = "";
		duration = Duration.between(this.time, time);
		int minutes = (int) (duration.getSeconds() / 60);
		
		if(minutes>60){
			horas = minutes/60;
			s = " "+horas+"h";	
			return s;
		}else if(minutes>0){
			s = " "+minutes+"m";
			return s;
		}else{
			s = null;
			return s;
		}
	}
}
