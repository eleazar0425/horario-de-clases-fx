package horario.clases.fx.model;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Materia {

	private StringProperty nombre, curso, profesor, hora;
	
	private int[] dias;
	
	public Materia(String hora, String nombre, String curso, String profesor){
		this.hora = new SimpleStringProperty(hora);
		this.nombre = new SimpleStringProperty(nombre);
		this.curso = new SimpleStringProperty(curso);
		this.profesor = new SimpleStringProperty(profesor);
	}
	
	public Materia(){
		
	}
	
	public void setNombre(String nombre){
		this.nombre.set(nombre);
	}
	
	public void setCurso(String curso){
		this.curso.set(curso);
	}
	
	public String getNombre(){
		return this.nombre.get();
	}
	
	public String getCurso(){
		return this.curso.get();
	}
	
	public void setDia(int i){
		dias = new int[1];
		dias[0] = i;
	}
	
	public void setDia(int i, int u){
		dias = new int[2];
		dias[0] = i;
		dias[1] = u;
	}
	
	public void setDia(int ... i){
		if(i.length<=7){
			dias = i;
		}else{
			System.out.println("Entrada invalida");
		}
	}
	
	public int[] getDias(){
		return dias;
	}

	public String getProfesor() {
		return profesor.get();
	}

	public void setProfesor(String profesor) {
		this.profesor.set(profesor);
	}

	public String getHora() {
		return hora.get();
	}

	public void setHora(String hora) {
		this.hora.set(hora);
	}
}
