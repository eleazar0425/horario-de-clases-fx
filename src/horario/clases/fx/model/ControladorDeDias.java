package horario.clases.fx.model;

import java.time.DayOfWeek;
import java.time.LocalDateTime;

import javafx.collections.ObservableList;

//CLASE PARA EL MANEJO DE DIAS

public class ControladorDeDias {
	
	private static DayOfWeek actualday; //VARIABLE QUE GUARDA EL DIA ACTUAL
	private static LocalDateTime localDate;
	
	public ControladorDeDias(){
	}
	
	//@return Devuelve el dia actual pero en base a numeros (Debe restarsele 1 para que quede en base 0a6
	public static int getActualDay(){
		localDate = LocalDateTime.now();
		actualday = localDate.getDayOfWeek();
		return
				actualday.getValue();
	}
	
	//Obtiene el dia actual, pero en un String con el nombre
	public static String getActualDayToString(){
		return ControladorDeDias.getDayByNum(getActualDay());
	}
	
	//Concierte el dia en formato int a formato String
	public static String getDayByNum(int num){
		switch(num){
		case 0:
			return "Lunes";
		case 1:
			return "Martes";
		case 2:
			return "Miercoles";
		case 3:
			return "Jueves";
		case 4:
			return "Viernes";
		case 5:
			return "Sabado";
		case 6:
			return "Domingo";
		default:
			return null;
		}
	}
}
