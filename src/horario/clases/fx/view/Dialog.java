package horario.clases.fx.view;

import horario.clases.fx.MainApp;
import horario.clases.fx.model.MateriaDB;

import java.util.List;
import java.util.Optional;
import java.util.Vector;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class Dialog {

	private String tittle, headerText, contentText;
	private MateriaDB materiaDB = new MateriaDB();
	
	public Dialog(String tittle, String headerText, String contentText){
		this.tittle = tittle;
		this.headerText = headerText;
		this.contentText = contentText;
	}

	public void Show(){
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setGraphic(new ImageView("file:res/dialog-information.png"));
		alert.initOwner(MainApp.getStage());
		alert.setTitle(tittle);
		alert.setHeaderText(headerText);
		alert.setContentText(contentText);
		alert.initModality(Modality.APPLICATION_MODAL);
		alert.showAndWait();
	}
	
	public void showError(){
		Alert alert = new Alert(AlertType.ERROR);
		alert.setGraphic(new ImageView("file:res/dialog-error.png"));
		alert.setTitle(tittle);
		alert.setHeaderText(headerText);
		alert.setContentText(contentText);
		alert.initModality(Modality.APPLICATION_MODAL);
		alert.showAndWait();
	}
	
	public void showDelete(String nombre, int diaActual, String hora){
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setGraphic(new ImageView("file:res/dialog-confirm.png"));
		alert.setTitle(tittle);
		alert.setHeaderText(headerText);
		alert.setContentText(contentText);
		alert.initModality(Modality.APPLICATION_MODAL);
		Optional<ButtonType> result = alert.showAndWait();
		
		int dia = diaActual;
		
		if(result.get() == ButtonType.OK){
			materiaDB.borrarHorario(dia, nombre, hora);
		}else{
			alert.close();
		}
	}
	
	public void showDeleteMateria(){
		MateriaDB materia = new MateriaDB();
		Vector<String> list = materia.getAllMaterias();
		String first = list.get(0);
		ChoiceDialog<String> dialog = new ChoiceDialog<>(first,list);
		dialog.setTitle(tittle);
		dialog.setHeaderText(headerText);
		dialog.setContentText(contentText);
		dialog.setGraphic(new ImageView("file:res/dialog-information.png"));
		Optional<String> result = dialog.showAndWait();
		if(result.isPresent()){
			materia.deleteMateria(result.get());
		}
	}
}
