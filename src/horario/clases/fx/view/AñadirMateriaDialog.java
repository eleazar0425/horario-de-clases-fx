
package horario.clases.fx.view;

import horario.clases.fx.MainApp;
import horario.clases.fx.controller.AņadirHorarioPaneController;
import horario.clases.fx.model.MateriaDB;

import java.util.Optional;

import javafx.application.Application;
import javafx.scene.control.TextInputDialog;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

public class AņadirMateriaDialog {

	TextInputDialog dialog;
	
	public AņadirMateriaDialog(){
		dialog = new TextInputDialog("");
		dialog.setTitle("Aņadir nueva materia");
		dialog.setHeaderText("Aņadir materia");
		dialog.setContentText("Por favor escriba el nombre");
		dialog.setGraphic(new ImageView("file:res/dialog-information.png"));
		dialog.initOwner(MainApp.getStage());
	}
	
	public void show(){
		Optional<String> result = dialog.showAndWait();
		if(result.isPresent()){
			final String result2 = result.get();
			
			if(result2!=null){
				MateriaDB materiadb = new MateriaDB();
				materiadb.addMateria(result2);
				AņadirHorarioPaneController ahpc = AņadirHorarioPaneController.getController();
				ahpc.initAņadirHorarioPane(result2);
			}
		}else{
			Dialog dialog = new Dialog("Error", "Por favor introduzca un nombre para la materia", "Intentelo de nuevo");
			dialog.Show();
		}
	}
}